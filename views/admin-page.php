<div class="wrap">

    <h1>Import/Export roles</h1>
    <?php if ($message): ?>
        <div id="message" class="updated notice notice-success is-dismissible">
            <p><?php echo $message; ?></p>
            <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
        </div>
    <?php endif; ?>
    <?php if ($error): ?>
        <div id="message" class="error notice notice-error is-dismissible">
            <p><?php echo $error; ?></p>
            <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
        </div>
    <?php endif; ?>
    <form method="post" enctype="multipart/form-data">
        <h4><a href="<?php echo $url; ?>">Export All Roles</a></h4>
        <p><strong>Or</strong></p>
        <h4>Import Roles</h4>
        <?php wp_nonce_field(plugin_basename(CUSTOM_EXPORTER_FILENAME), 'roles_import'); ?>
        <table>
            <tbody>
            <tr>
                <th scope="row"><label for="blogname">Select file</label></th>
                <td><input type="file" name="json_file" multiple="false" accept=".json"></td>
            </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="submit-json-roles" class="button button-primary">
        </p>
    </form>
</div>

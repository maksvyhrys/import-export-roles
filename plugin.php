<?php
/**
 * Plugin Name: Roles import/export
 * Plugin URI: http://localhost/
 * Description: Roles import/export
 * Version: 1.0
 * Author: Maksim Vyhryst
 * Author URI: http://localhost/
 */

define('CUSTOM_EXPORTER_FILENAME', __FILE__);

add_action('admin_menu', function () {
    add_menu_page(
        'Roles export',
        'Roles export',
        'edit_posts',
        'export-roles',
        'custom_roles_export_import'
    );
});

function custom_roles_export_import() {
    $url = admin_url('admin.php?page=export-roles&export-all-roles=true');
    $url = wp_nonce_url($url, 'export_roles', 'export_nonce');
    if ($_FILES['json_file'] and $_FILES['json_file']['error'] === 0 && check_admin_referer(plugin_basename(CUSTOM_EXPORTER_FILENAME), 'roles_import'))
    {
        $result = process_import_from_file();
        $error = $result['error'];
        $message = $result['message'];
    }
    require_once __DIR__ . '/views/admin-page.php';
}

function import_roles($roles) {
    if (!is_array($roles) || empty($roles))
        throw new Exception('Roles should be not empty array');

    $results = [];
    foreach ($roles as $slug => $role) {
        if (!isset($role['name']) || !isset($role['capabilities']))
            throw new Exception('Invalid data format');

        if (null !== get_role($slug)) remove_role($slug);
        $results[] = add_role($slug, $role['name'], $role['capabilities']);
    }

    return count($results) === count($roles);
}

function process_import_from_file() {
    $result = [ 'message' => false, 'error' => false ];
    try {
        $json = file_get_contents($_FILES['json_file']['tmp_name']);
        $roles = json_decode($json, true);
        if (import_roles($roles)) {
            $result['message'] = 'All roles were successfully imported';
        } else {
            $result['error'] = 'Something went wrong';
        }
    } catch (Exception $exception) {
        $result['error'] = $exception->getMessage();
    }
    return $result;
}

function get_all_roles_json() {
    $all_roles = wp_roles()->roles;
    $json = json_encode($all_roles, JSON_PRETTY_PRINT);
    return $json;
}

add_action('init', function () {
    if (isset($_GET['export-all-roles']) && wp_verify_nonce($_GET['export_nonce'], 'export_roles')) {
        $time = time();
        $filename = "roles-export-$time.json";
        fwrite(fopen("/tmp/$filename", 'w'), get_all_roles_json());
        header("Expires: 0");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header('Cache-Control: pre-check=0, post-check=0, max-age=0', false);
        header("Pragma: no-cache");
        header("Content-type: application/json");
        header("Content-Disposition:attachment; filename={$filename}");
        header("Content-Type: application/force-download");
        readfile("/tmp/$filename");
        exit();
    }
});